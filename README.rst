==========
Brain_Wash
==========

Brain_Wash is a simple Django app that pushes users to read faster.  In the future, this app will be able to save the texts the user feeds to it as well as where in the text the reader is at.

Quick start
-----------

1. Add "brain_wash" to your INSTALLED_APPS setting like this::

     INSTALLED_APPS = [
     ...
     'brain_wash',
     ]

2. Include the brain_wash URLconf in your project urls.py like this::

     path('brain_wash/', include('brain_wash.urls')),

3. Run `python manage.py migrate` to create the brain_wash models.

4. Start the development server and visit http://127.0.0.1:8000/brain_wash/ to start using the app.
