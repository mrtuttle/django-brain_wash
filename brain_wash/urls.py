from django.urls import path
from django.views import generic

app_name = 'brain_wash'
urlpatterns = [
    path('', generic.TemplateView.as_view(template_name='brain_wash/home.html'), name='home'),
]
