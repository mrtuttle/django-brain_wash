var state = {
    wpm: 60,
    paused: true,
    time_out_id: 0,    
    text: '',
    paragraphs: [],
    paragraph_index: 0,
    words: [],
    word_index: 0,
}

function wpm_up () {
    clearTimeout(state.time_out_id)
    state.wpm = Math.round(state.wpm * (11/10))
    $('#wpm').text(state.wpm)
    if (!state.paused) state.time_out_id = setTimeout(get_word, 60000/state.wpm)
}

function wpm_down () {
    clearTimeout(state.time_out_id)
    state.wpm = Math.round(state.wpm * (10/11))
    if (state.wpm < 60) state.wpm = 60
    $('#wpm').text(state.wpm)
    if (!state.paused) state.time_out_id = setTimeout(get_word, 60000/state.wpm)
}

function make_paused () {
    clearTimeout(state.time_out_id)
    state.paused = true
    $('#paused').text('PAUSED')
}

function toggle_paused () {
    state.paused = !state.paused
    if (state.paused) {
        make_paused()
    } else {
        $('#paused').text('Pause')
        get_word()
    }
}

function previous_paragraph () {
    make_paused()
    state.paragraph_index = state.paragraph_index - 2
    if (state.paragraph_index < 0) state.paragraph_index = 0
    setup_word_bank()
}

function next_paragraph () {
    make_paused()
    if (state.paragraph_index < state.paragraphs.length) {
        setup_word_bank()
    } else {
        setup_paragraph_bank()
    }
}

function get_word () {
    if (state.word_index < state.words.length) {
        $('#word_holder').text(state.words[state.word_index])
        $('#word_' + state.word_index).attr('class', 'active')
        state.word_index++
        state.time_out_id = setTimeout(get_word, 60000/state.wpm)
    } else {
        next_paragraph()
    }
}

function setup_word_bank () {
    state.words =  state.paragraphs[state.paragraph_index].split(' ')
    state.word_index = 0
    $('#word_holder').text(state.words[state.word_index])
    $('#paragraph_index').text(state.paragraph_index + 1)
    $('#paragraph_holder').empty()
    for (var index = 0; index < state.words.length; index++) {
        $('#paragraph_holder').append($(
            '<span id="word_' + index + '">' + state.words[index] + ' </span>'
        ))
    }
    state.paragraph_index++
}

function setup_paragraph_bank () {
    make_paused()
    state.text = $('#word_source').val()
    state.paragraphs = state.text.split('\n\n')
    state.paragraph_index = 0
    setup_word_bank()
}

$(function () {
    $('#app_display').bind('keydown', function (e) {
        if (e.which == 32) { // spacebar
            e.preventDefault()
            toggle_paused()
        }
        if (e.which == 37) { // left
            e.preventDefault()
            previous_paragraph()
        }
        if (e.which == 39) { // right
            e.preventDefault()
            next_paragraph()
        }
        if (e.which == 38) { // up
            e.preventDefault()
            wpm_up()
        }
        if (e.which == 40) { // down
            e.preventDefault()
            wpm_down()
        }
    })

    $('#wpm').text(state.wpm)
    setup_paragraph_bank()
})
