from django.apps import AppConfig


class BrainWashConfig(AppConfig):
    name = 'brain_wash'
